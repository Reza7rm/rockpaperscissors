/**
 * @format
 */

import 'react-native';
import React from 'react';
import Home from '../src/screens/Home';
import {findWinner} from '../src/helpers/game-logic'
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  renderer.create(<Home />);
});

//game logic chack
it('rock wins scissors', () => {
  expect(findWinner('rock','scissors')).toBe("Player1");
});

it('paper wins rock', () => {
  expect(findWinner('paper','rock')).toBe("Player1");
});

it('scissors wins paper', () => {
  expect(findWinner('scissors','paper')).toBe("Player1");
});

//game logic chack by changing parameters order
it('rock wins scissors', () => {
  expect(findWinner('scissors','rock')).toBe("Player2");
});

it('paper wins rock', () => {
  expect(findWinner('rock','paper')).toBe("Player2");
});

it('scissors wins paper', () => {
  expect(findWinner('paper','scissors')).toBe("Player2");
});

it('null check for player 2 (as human) when computer has selected rock', () => {
  expect(findWinner('rock',null)).toBe("Player1");
});

it('null check for player 2 (as human) when computer has selected paper', () => {
  expect(findWinner('paper',null)).toBe("Player1");
});

it('null check for player 2 (as human) when computer has selected scissors', () => {
  expect(findWinner('scissors',null)).toBe("Player1");
});

