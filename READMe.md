# Rock Paper Scissors

Rock Paper Scissors is a simple game written using react-native for android(and iOS, but not tested)

## Installation
To install react native you may refer to   [react native official docs ](https://facebook.github.io/react-native/docs/getting-started)  
if you have already installed react-native and all necessary libraries you may follow the installation 
 
Using npm

```bash
npm install
```
Using yarn

```bash
yarn 
```

then:
```bash
react-native run-android
```

## Test
Some Sample tests are written and to run them you may use npm or yarn
using npm
```bash
npm test

```
using yarn
```bash
yarn test

```
## Rules
rock wins scissors
scissors wins paper
paper wins rock

In player vs computer mode, if you don't select your shape before the timer ends you will lose.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)