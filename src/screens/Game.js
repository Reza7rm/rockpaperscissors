import React, { Component } from 'react'
import { View, Text, StatusBar, StyleSheet,Image } from "react-native"
import Timer from "../components/Timer"
import ShapeSelector from "../components/ShapeSelector"
import { findWinner, randomSelect } from "../helpers/game-logic"
import CustomButton from "../components/CustomButton"

// statically analyzed images

const IMAGES = {
    "rockwins": require('../images/rockwins.png'), 
    "paperwins": require('../images/paperwins.png'), 
    "scissorswins":require('../images/scissorswins.png') 
}

export default class Game extends Component {
    constructor(props) {
        super(props)
        this.gameType = this.props.navigation.getParam("gameType")
        this.state = {
            counter:5,
            active: true,
            finishEvent: null,
            player1Shape:null,
            player2Shape: null,
            winner: null,
            winnerShape:null,
            gameType: this.props.navigation.getParam("gameType")
        }
        //this.props.navigation.dispatch(resetAction)
    }

    changeShape = (playerShape, shape) => {
        if (this.state.active) {
            this.setState({ [playerShape]: shape })
        }
    }

    defineCounter=()=>{
        return this.state.gameType === "C-vs-C" ? 3 : 5
    }

    definePlayerName=(player)=>{
        if(player == "Player1"){
            return this.gameType === "C-vs-C" ? "Player 1" :"Computer"
        }else if (player == "Player2" ){
            return this.gameType === "P-vs-C" ? "You" :"Player 2"
        }
    }

    defineWinner = () => {
        let winner = findWinner(this.state.player1Shape, this.state.player2Shape)
        this.setState({ winner })

        let theWinnerShape = winner=="Player1" ? this.state.player1Shape :this.state.player2Shape;
        this.setState({winnerShape: theWinnerShape})
    }

    finishGame = () => {
        this.setState({ active: false })

        if(this.gameType == "C-vs-C"){
            this.setState({player1Shape:randomSelect(),player2Shape:randomSelect()},()=>this.defineWinner())
        }else{
            this.setState({player1Shape:randomSelect()},()=>this.defineWinner())
        }

    }

    reset=()=>{
        this.setState({active:true,counter:this.defineCounter(),player1Shape:null,player2Shape:null})
    }

    componentDidMount() {


    }

    render() {
        return (
            <View style={styles.container}>
            <StatusBar backgroundColor="black"/>
                <View style={{ width: '80%', flex: 1, justifyContent: "space-around" }}>
                    <ShapeSelector 
                    name= {this.definePlayerName("Player1")}
                    auto={true} shapeState="player1Shape" 
                    shape={this.state.player1Shape} 
                    shapeChanger={this.changeShape} 
                    
                    />

                    {this.state.active &&
                        <View style={{ justifyContent: "center", alignItems: "center" }}>
                            <Timer counter={this.defineCounter()} finishEvent={() => { this.finishGame() }} />
                        </View>
                    }

                    {!this.state.active && 
                        <View style={{justifyContent:"center",alignItems:"center"}}>
                            <Text style={styles.winnerText}>{this.state.winner != "Tie"? (this.definePlayerName(this.state.winner)+" won!"): "Tie"} </Text>
                            { this.state.winner != "Tie" && <Image style={{ width: 100, height: 100 }} source={IMAGES[this.state.winnerShape + "wins"]}></Image>}

                            <View style ={{flexDirection:"row",justifyContent:"space-between"}}>
                            <CustomButton style={styles.btnStyle} textStyle={styles.btnText}  onPress={()=>this.reset()} title="Retry" />
                            <View style={{width:10}}></View>
                            <CustomButton style={styles.btnStyle} textStyle={styles.btnText} onPress={()=>this.props.navigation.navigate("Home")} title="Change Mode" />

                            </View>
                        </View>
                    }

                    <ShapeSelector 
                    name= {this.definePlayerName("Player2")}
                    auto={this.state.gameType=="C-vs-C"?true:false} 
                    shapeState="player2Shape" 
                    shape={this.state.player2Shape} 
                    shapeChanger={this.changeShape} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    btnContainer: {
        margin: 10
    },
    btnStyle:{
        borderRadius:10,
        height:30,
        width:130,
    },
    btnText:{
      fontSize:14
    },
    winnerText:{
        color:"green",
        alignSelf:"center",
        fontWeight:"bold",
        fontSize:20
    }
});
