import React, { Component } from 'react'
import {View, StatusBar,StyleSheet} from "react-native";
import CustomButton from "../components/CustomButton"
export default class Home extends Component {
    render() {
        return (
            <View style={styles.container}>
            <StatusBar backgroundColor="black"/>

                <View style={{width:'80%',flex:1,justifyContent:"center"}}>
                    <View style={styles.btnContainer}>
                        <CustomButton  onPress={()=>this.props.navigation.navigate("Game",{gameType:"P-vs-C"})}  title="Player VS Computer" />
                    </View>
                    <View style={styles.btnContainer}>
                        <CustomButton onPress={()=>this.props.navigation.navigate("Game",{gameType:"C-vs-C"})} title="Computer VS Computer" />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1, 
        justifyContent:"center",
        alignItems:"center"
    },
    btnContainer:{
        margin:10
    },
  });
