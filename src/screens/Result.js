import React, { Component } from 'react'
import { Text, View, Image,StyleSheet } from 'react-native'
const IMAGES = {
    "rockwins": require('../images/rockwins.png'), // statically analyzed
    "paperwins": require('../images/paperwins.png'), // statically analyzed
    "scissorswins":require('../images/scissorswins.png')
  }
export class Result extends Component {
    constructor(props) {
        super(props)
        this.winner = this.props.navigation.getParam("winner")
        this.winnerShape = this.props.navigation.getParam("winnerShape")
    }
    render() {
        return (
            <View style={styles.container}>
           {this.winner != "Tie" && <View >
                <Text style={styles.winnerText}>{this.winner} wins</Text>
                <Image style={{width:200,height:200}} source ={IMAGES[this.winnerShape+"wins"]}></Image>
            </View>}
            </View>
            
        )
    }
}

export default Result

const styles = StyleSheet.create({
    container: {
        flex:1, 
        justifyContent:"center",
        alignItems:"center"
    },
    winnerText:{
        color:"green",
        alignSelf:"center",
        fontWeight:"bold",
        fontSize:30
    },
  });