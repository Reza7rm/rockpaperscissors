export function findWinner(player1_Selection,player2_Selection){
    
    const elementWinner = {"scissors":"rock", "rock":"paper", "paper":"scissors"}
    
    if(player1_Selection === player2_Selection) return "Tie";
    
    return  (elementWinner[player1_Selection] === player2_Selection) ? "Player2" : "Player1"
}

export function randomSelect(){
    
    const elements = ["scissors","rock","paper"]
    let index = Math.floor(Math.random() * 3); 
    return elements[index]
}