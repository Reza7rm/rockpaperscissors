import React, { Component } from 'react'
import { Text, StyleSheet, View ,TouchableOpacity} from 'react-native'
import { setInterval } from 'core-js';
import {randomSelect} from "../helpers/game-logic"
import HandShape from "../components/HandShape"
export default class ShapeSelector extends Component {
    constructor(props) {
        super(props)
        const {shape} = props
        this.interval = null
        this.state = {
            selectedShape:shape
        }
        //this.props.navigation.dispatch(resetAction)
    }

    changeShape=(shape)=>{
        if(!this.props.auto){
            this.props.shapeChanger(this.props.shapeState,shape)
        }
    }

    defineActiveOpacity=()=>{
        return this.props.auto == true ? 1 : 0.5
    }
    componentDidMount(){

    }
    componentWillUnmount(){
        clearInterval(this.interval)
    }

    render() {
        return (
            <View>
            <Text style={styles.playerName}>{this.props.name}</Text>
            <View style={styles.container}>
                <TouchableOpacity activeOpacity={this.defineActiveOpacity()} onPress={()=>this.changeShape("rock")}>
                    <HandShape shape ="rock"  selectedShape={this.props.shape}></HandShape>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={this.defineActiveOpacity()} onPress={()=>this.changeShape("paper")}>
                    <HandShape shape ="paper"  selectedShape={this.props.shape}></HandShape>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={this.defineActiveOpacity()} onPress={()=>this.changeShape("scissors")}>
                    <HandShape shape ="scissors"  selectedShape={this.props.shape}></HandShape>
                </TouchableOpacity>
            </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        borderRadius:15,
        borderWidth:1,
        borderColor:"black",
        flexDirection:"row",
        justifyContent:"space-between",
        padding:15
    },
    playerName:{
        alignSelf:"center",
        fontWeight:"bold",
        padding:10,
    }
})

