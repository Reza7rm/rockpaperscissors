import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { setInterval } from 'core-js';

export default class Timer extends Component {
    constructor(props) {
        super(props)
        const {counter,finishEvent} = props
        this.interval = null
        this.state = {
            counter,
            finishEvent
        }
        //this.props.navigation.dispatch(resetAction)
    }
    tickDown = ()=>{
        if (this.state.counter > 0){
            this.setState({counter:this.state.counter - 1})
        }else{
            this.state.finishEvent()
            clearInterval(this.interval)
        }
    }
    componentDidMount(){
        this.interval=setInterval(this.tickDown,1000)
    }
    componentWillUnmount(){
        clearInterval(this.interval)
    }



    render() {
        return (
            <View style={{height:120,width:120,borderRadius:60,backgroundColor:"#11dd77",justifyContent:"center",alignItems:"center",borderWidth:1}}>
                <Text style={{fontSize:70}}> {this.state.counter} </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({})

