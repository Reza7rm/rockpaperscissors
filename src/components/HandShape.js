import React from 'react'
import { View, Text,Image,TouchableOpacity } from 'react-native'

// statically analyzed Images
const IMAGES = {
    "rock": require('../images/rock.png'), 
    "paper": require('../images/paper.png'), 
    "scissors":require('../images/scissors.png')
  }

 HandShape=(props) => {

    return (
        <View style={{backgroundColor: props.selectedShape == props.shape ? "#11DD55" : "#D2D2D2"
        ,width:70,height:70, borderRadius:35,justifyContent:"center",alignItems:"center"}}>
             <Image style={{width:50,height:50}} source ={IMAGES[props.shape]} ></Image> 
        </View>
    )
}

export default HandShape
