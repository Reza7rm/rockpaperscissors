import React from "react";
import Layout from "./router";

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.props.style = { backgroundColor: 'white' }
  }


  render() {
    //const Layout = RootNavigator();
    return <Layout style={{ flex: 1 }} />;
  }
}
