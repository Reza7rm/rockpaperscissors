
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Home from "./screens/Home"
import Game from "./screens/Game"
import Result from "./screens/Result"


const AppNavigator = createStackNavigator({
    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => ({
        title: 'Rock-Paper-Scissors',
      }),
    },
    Game: {
      screen: Game,
      navigationOptions: ({ navigation }) => ({
        title: 'Rock-Paper-Scissors'
      }),
    
    },

    Result: {
      screen: Result,
      navigationOptions: ({ navigation }) => ({
        title: 'Result',
      }),
    },
  },
  {
    defaultNavigationOptions: {
      headerTintColor: "white",
      headerStyle: { borderBottomWidth: 0, backgroundColor: "black" },
      headerTitleStyle: { color: "white" },
    },
    headerLayoutPreset :"center"
  }
  );
  
  export default createAppContainer(AppNavigator);